﻿namespace WinFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int n = int.Parse(textBox1.Text);
            int m = int.Parse(textBox2.Text);

            dataGridView1.RowCount = n;
            dataGridView1.ColumnCount = m;
            dataGridView3.RowCount = n;
            dataGridView3.ColumnCount = m;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                for (int j = 0; j < dataGridView1.ColumnCount; j++)
                {
                    if (Convert.ToInt32(dataGridView1.Rows[i].Cells[j].Value) == 0)
                    {
                        dataGridView1.Rows[i].Cells[j].Value = dataGridView3.Rows[i].Cells[j].Value; // присвоїти нове значення клітинці
                    }
                }
            }
        }
    }
}